Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: HotSpot with Dynamic Code Evolution VM
Source: https://github.com/HotswapProjects/openjdk-jdk11u-dcevm
Files-Excluded: make/data/*
                src/bsd/*
                src/demo/*
                src/java.compiler/*
                src/java.datatransfer/*
                src/java.desktop/*
                src/java.instrument/*
                src/java.logging/*
                src/java.management*
                src/java.naming/*
                src/java.net.http/*
                src/java.prefs/*
                src/java.rmi/*
                src/java.scripting/*
                src/java.se/*
                src/java.security*
                src/java.smartcardio/*
                src/java.sql*
                src/java.transaction.xa/*
                src/java.xml*
                src/jdk.*
                src/linux/*
                src/sample/*
                src/solaris/*
                src/utils/*
                test/jaxp/*
                test/jdk/*
                test/langtools/*
                test/lib/*
                test/nashorn/*

Files: *
Copyright: 2007-2019, Oracle and/or its affiliates
           2012, Thomas Wuerthinger
           2014-2015, Ivan Dubrov
           2014-2019, Vladimir Dvorak
           2018, Przemysław Rumik
License: GPL-2 with Classpath exception

Files: test/fmw/*
Copyright: 2003-2009, Google Inc
License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
     * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above
 copyright notice, this list of conditions and the following disclaimer
 in the documentation and/or other materials provided with the
 distribution.
     * Neither the name of Google Inc. nor the names of its
 contributors may be used to endorse or promote products derived from
 this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: doc/nashorn/source/*
       make/jdk/netbeans/*
       make/langtools/netbeans/langtools/*
       src/java.base/share/legal/asm.md
       src/java.base/share/native/libjimage/*
Copyright: 2007-2018, Oracle and/or its affiliates
License: BSD-3-clause-oracle
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
   - Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
 .
   - Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
 .
   - Neither the name of Oracle nor the names of its
     contributors may be used to endorse or promote products derived
     from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: debian/*
Copyright: 2014-2019, Emmanuel Bourg <ebourg@apache.org>
License: GPL-2

License: GPL-2
 On Debian systems, the complete text of the GNU General Public License
 can be found in `/usr/share/common-licenses/GPL-2'.

License: GPL-2 with Classpath exception
 This code is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License version 2 only, as
 published by the Free Software Foundation.  Oracle designates this
 particular file as subject to the "Classpath" exception as provided
 by Oracle in the LICENSE file that accompanied this code.
 .
 This code is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 version 2 for more details (a copy is included in the LICENSE file that
 accompanied this code).
 .
 You should have received a copy of the GNU General Public License version
 2 along with this work; if not, write to the Free Software Foundation,
 Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 or visit www.oracle.com if you need additional information or have any
 questions.
 .
 On Debian systems, the complete text of the GNU General Public License
 can be found in `/usr/share/common-licenses/GPL-2'.
 .
 "CLASSPATH" EXCEPTION TO THE GPL VERSION 2
 .
 Certain source files distributed by Oracle, Inc. are subject
 to the following clarification and special exception to the GPL, but
 only where Oracle has expressly included in the particular source file's
 header the words "Oracle designates this particular file as subject to
 the "Classpath" exception as provided by Oracle in the LICENSE file that
 accompanied this code."
 Linking this library statically or dynamically with other modules is
 making a combined work based on this library. Thus, the terms and
 conditions of the GNU General Public License cover the whole combination.
 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent modules,
 and to copy and distribute the resulting executable under terms of your
 choice, provided that you also meet, for each linked independent module,
 the terms and conditions of the license of that module. An independent
 module is a module which is not derived from or based on this library.
 If you modify this library, you may extend this exception to your version
 of the library, but you are not obligated to do so. If you do not wish
 to do so, delete this exception statement from your version.

